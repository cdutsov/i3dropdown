#include <inttypes.h>
#include <string.h>
#include <emmintrin.h>
#include <pmmintrin.h>

#ifndef __PREDIQUERY_H_
#define __PREDIQUERY_H_

#define PREDIQUERY_DEBUG 0
#if PREDIQUERY_DEBUG > 0
  #include "debug_prediquery.h"
#endif

namespace prediquery {

typedef uint64_t ExprMask;

namespace EXPR_FLAG{
    const uint32_t INV = 1 << 31;
    const uint32_t REC = 1 << 30;
    const uint32_t OR = 1 << 29;
    const uint32_t RAISE = 1 << 28;
    const uint32_t APP = 1 << 27;
    const uint32_t PRED = 1 << 26;
    const uint32_t DUMMY = 1 << 25;
    const uint32_t RAISE_RELATED = 1 << 24;
    const uint32_t REC_OFFSET = 8; 
    const uint32_t FLAG_SCOPE = (~0u) << 8;
    
}

namespace EXPR_FLAG{
    enum type{
        BOOL = 0,
        UINT = 1,
        STRING = 2,
        STRING_PREFIX = 3,
        INT
    };
}

// Expression Types
//  Base-Predicate(Applicable): name, type, value, rel
//  Meta-Predicate: meta, type, value, rel
//  Invoice(Applicable): name, type = INV, dest, rel 
//      if type has INV-OR then Invoice is statisfied if any in rel
//      if type has INV-AND then Invoice is statisfied if all in rel
//      INV-AND and empty implies always statisfied.
//  Recursive(Applicable): name, type = Rec, rel, value 
//      
struct QueryExpr{
    uint32_t type_mask;
    int name_len;
    union {
        ExprMask meta;
        const char *name;
    };
    union {
        uint64_t value;
        const char *str_value;
        char **str_dest;
        uint64_t *uint_dest;
        int64_t *int_dest;
    };
    ExprMask related;
};


const QueryExpr invoice(const char * name, int64_t *int_loc, ExprMask rel = 0ul, uint32_t type_mask = 0){
    return QueryExpr{.type_mask = type_mask |EXPR_FLAG::INV | EXPR_FLAG::APP | EXPR_FLAG::UINT,
            .name_len = (int)strlen(name), .name = name, .int_dest = int_loc, .related = rel};
}
const QueryExpr invoice(const char * name, uint64_t *int_loc, ExprMask rel = 0ul, uint32_t type_mask = 0){
    return QueryExpr{.type_mask = type_mask | EXPR_FLAG::INV | EXPR_FLAG::APP | EXPR_FLAG::UINT,
            .name_len = (int)strlen(name), .name = name, .uint_dest = int_loc, .related = rel};
}

const QueryExpr invoice(const char * name, char **str_loc, ExprMask rel = 0ul, uint32_t type_mask = 0){
    return QueryExpr{.type_mask = type_mask | EXPR_FLAG::INV | EXPR_FLAG::APP | EXPR_FLAG::STRING,
            .name_len = (int)strlen(name), .name = name, .str_dest = str_loc, .related = rel};
}

inline constexpr uint32_t raise_index( uint32_t type_mask ){
    return (type_mask >> EXPR_FLAG::REC_OFFSET) & ((1 << 8)-1);
}   

inline constexpr uint32_t raise_flag(unsigned int index){
    return (index << EXPR_FLAG::REC_OFFSET) | EXPR_FLAG::RAISE;
}   

const QueryExpr predicate(ExprMask value, ExprMask rel ,uint32_t type){
    return QueryExpr{.type_mask = type | EXPR_FLAG::PRED, .value = value,  .related = rel};
}

const QueryExpr predicate(const char *name, const char *str_value, ExprMask rel = 0ul, uint32_t type=0){
    return QueryExpr{.type_mask = type | EXPR_FLAG::PRED | EXPR_FLAG::APP,
            .name_len = (int)strlen(name), .name = name, .str_value = str_value, .related = rel};
}

const QueryExpr dummy(ExprMask rel, uint32_t type_mask){
    return QueryExpr{.type_mask = type_mask | EXPR_FLAG::PRED | EXPR_FLAG::DUMMY,
            .related = rel};
}
const QueryExpr predicate(const char *name, ExprMask value, ExprMask rel=0ul, uint32_t type =0){
    return QueryExpr{.type_mask = type | EXPR_FLAG::PRED | EXPR_FLAG::APP,
            .name_len = (int)strlen(name), .name = name, .value = value, .related = rel};
}

const QueryExpr recurse(const char * name,  ExprMask rel){
    return QueryExpr{.type_mask = EXPR_FLAG::REC | EXPR_FLAG::APP,
            .name_len = (int)strlen(name), .name = name, .related = rel};
}

template<typename ...T>
constexpr ExprMask bitset(T ...a){
    return ((1 << a) ^ ... ^ 0);
}


int bit_index(uint64_t bits, int index){
   return __builtin_popcountl(bits & (~((~0ul) << index)));
}

    namespace lookup{
        static constexpr uint8_t table[256] = {
         // 0    1    2    3    4    5    6    7      8    9    A    B    C    D    E    F
            5,   0,   0,   0,   0,   0,   0,   0,     0,   0,   2,   2,   2,   2,   0,   0, // 0
            0,   0,   0,   0,   0,   0,   0,   0,     0,   0,   0,   0,   0,   0,   0,   0, // 1
            2,   0,   7,   0,   0,   0,   0,   0,     0,   0,   0,   0,   3,   0,   0,   0, // 2
            0,   0,   0,   0,   0,   0,   0,   0,     0,   0,   0,   0,   0,   0,   0,   0, // 3
            0,   0,   0,   0,   0,   0,   0,   0,     0,   0,   0,   0,   0,   0,   0,   0, // 4
            0,   0,   0,   0,   0,   0,   0,   0,     0,   0,   0,   6,   1,   4,   0,   0, // 5
            0,   0,   0,   0,   0,   0,   0,   0,     0,   0,   0,   0,   0,   0,   0,   0, // 6
            0,   0,   0,   0,   0,   0,   0,   0,     0,   0,   0,   6,   0,   4,   0,   0, // 7

            // 128-255
            0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0
        };
        const uint8_t OTHER = 0;
        const uint8_t BACK_SLASH = 1;
        const uint8_t WHITESPACE = 2;
        const uint8_t COMMA = 3;
        const uint8_t CLOSE_BRACE_OR_BRACKET = 4;
        const uint8_t NULL_CHAR = 5;
        const uint8_t OPEN_BRACE_OR_BRACKET = 6;
        const uint8_t DOUBLE_QUOTE = 7;
        inline uint8_t query(const char a){
            return table[static_cast<unsigned>(a)];
        }

        inline bool normal_dil(const char a){
            int val = table[static_cast<unsigned>(a)];
            switch (val){
                case WHITESPACE: return true;
                case COMMA: return true;
                case CLOSE_BRACE_OR_BRACKET: return true;
                default: return false;
            }
        }
    }

const char *ajstr_skip(const char *j){
    j++; 
    while(*j && *j != '"') j+= (*j == '\\') ? 2 : 1;
    return j;
}


const char *jstr_skip(const char *p){
    p++;
    const __m128i quote_mask = _mm_set1_epi8('"');
    const auto kill_mask = _mm_set1_epi8('\0');
    const auto esc_mask = _mm_set1_epi8('\\');
    unsigned index = 0;
    while (true) {
        __m128i block = _mm_lddqu_si128((const __m128i*)p);
        auto is_esc = _mm_cmpeq_epi8(block, esc_mask);
        auto is_quote = _mm_cmpeq_epi8(block, quote_mask);
        auto is_kill = _mm_cmpeq_epi8(block, kill_mask);
        auto mask = _mm_movemask_epi8(_mm_or_si128(_mm_or_si128(is_quote,is_kill),is_esc));
        if (mask != 0) {
            index = __builtin_ctz(mask);
            break;
        }
        p += 16;
    }

    if (*(p + index) == '"') return p + index;
    p = p + index -1;
    while(*p && *p != '"') p+= (*p == '\\') ? 2 : 1;
    return p;
}

inline const char * skip_whitespace(const char * q){
        while(lookup::query(*q) == lookup::WHITESPACE) ++q;
        return q;
}

const char *query_skip_dep(const char *j, int depth = 0){
    while (1) switch (lookup::query(*j)){
        case lookup::NULL_CHAR: //
            return j;
        case lookup::DOUBLE_QUOTE: //Char:"
            if (!(*(j = jstr_skip(j)))) return j;
            ++j;
            continue;
        case lookup::OPEN_BRACE_OR_BRACKET: //Char:[ or {
            ++depth;
            ++j;
            continue;
        case lookup::CLOSE_BRACE_OR_BRACKET://Char:} or ]
            if (--depth == 0) return j;
            ++j;
            continue;
        case lookup::OTHER:     //IGNORED
        case lookup::COMMA:     //
        case lookup::WHITESPACE://
        case lookup::BACK_SLASH:// 
             ++j;
             continue;
         default:
             __builtin_unreachable();
    };
}


// REQUIRES j proceeds an argument. 
// Moves the cursor forward once then will return the
// cursor after character fake_depth is zero or if the
const char *query_skip(const char *j){
    j--;
    while(1) switch (lookup::query(*(++j))){
        case lookup::DOUBLE_QUOTE: //Char:"
            if (!(*(j = jstr_skip(j)))) return j;
            break;
        case lookup::OPEN_BRACE_OR_BRACKET: //Char:[ or {
            if ( !(*(j = query_skip_dep(j+1, 1))) ) return j;
            break;
        case lookup::CLOSE_BRACE_OR_BRACKET://Char:} or ]
        case lookup::COMMA://Char:} or ]
        case lookup::NULL_CHAR://Char:} or ]
            return j;
        case lookup::OTHER:     //IGNORED
        case lookup::WHITESPACE://
        case lookup::BACK_SLASH:// 
            break;
        default:
             __builtin_unreachable();
    };

}
    
class IterateMask {
    uint64_t bitmask;
public:
    static uint64_t inline first_bit(const uint64_t bitmap){
        return bitmap ? __builtin_ctzl(bitmap) : 0;
    }
    class iter{
        uint64_t bitmap;
        explicit iter(uint64_t bitmap):
            bitmap{bitmap}  {}
    public:
        iter &operator++() {
            bitmap &= (bitmap -1);
            return *this;
        }
        uint64_t operator*() const { return first_bit(bitmap); }
        bool operator!=(const iter& rhs) const { return rhs.bitmap != bitmap; }
        friend IterateMask;
    };
    constexpr IterateMask(uint64_t bitmap): bitmask{bitmap}{}
    iter begin() const { return iter(bitmask); }
    iter end()   const { return iter(0);}
};


struct QueryLayer{
   ExprMask expr_mask; 
   ExprMask passed_mask; 
   ExprMask active_app_mask; 
   unsigned int site_depth; 
   unsigned int site_len; 
   bool array;
};

struct InqSite {
    uint32_t pos = 0;
};

bool compare_bool(bool value, const char *str){
    if (value == true) {
        if (str[0] != 't') return false;
        if (str[1] != 'r') return false;
        if (str[2] != 'u') return false;
        if (str[3] != 'e') return false;
        if (!lookup::normal_dil(str[4])) return false;
    } else if (value == false){
        if (str[0] != 'f') return false;
        if (str[1] != 'a') return false;
        if (str[2] != 'l') return false;
        if (str[4] != 's') return false;
        if (str[5] != 'e') return false;
        if (!lookup::normal_dil(str[6])) return false;
    }
    return true;
}
uint64_t parse_uint(const char *j){
    uint64_t num = 0;
    while(*j <= '9' && *j >= '0'){num = num * 10 + (*(j++) - '0');}
    return num;
}

int64_t parse_int(const char *j){
    int64_t num = 0;
    int64_t sign = 1;
    if (*j == '-') {
        sign = -1;
        ++j;
    }
    while(*j <= '9' && *j >= '0'){num = num * 10 + (*(j++) - '0');}
    return num*sign;
}
uint32_t startsWith( const char* prefix, const char* str, int len) {
    uint8_t _cp, _cs;
    const uint8_t* _pr = (uint8_t*) prefix;
    const uint8_t* _str = (uint8_t*) str;

    while ( ( _cs = *_str++ ) & ( _cp = *_pr++ ) && len--) {
        if ( _cp != _cs ) return 0;
    }
    if (len == -1) return ( _cp == _cs ) && _pr[1] == '\0';
    return (!_cp);
}
#define MAX_DEPTH 32
#define MAX_SITE_DEPTH 128
#define HASH_TABLE_SIZE 97


//  Quick and dirty prefix string hash
//  The table size and total alphbet is often very small
//  hash function must be very fast to seen any performance improvments
//  in small-medium queries.
//
//  - Will access no more than 3 characters after len.
//  - Often uses less then specifed len in hash. 
inline uint8_t hash_db(const char *str,unsigned int len){
    if (len > 4){
        uint64_t value = *reinterpret_cast<const uint64_t*>(str);

        // Shift Discard bits after len if less then 8
        //    otherwise discard 1-8 bits depending on length, so
        //    strings will same prefix but different length may
        //    have different hashes.
        uint64_t shift = ((8 - (len % 8)) << 1 << 2*!(len >> 3));
        uint64_t mask;
        if (__BYTE_ORDER__ == __ORDER_BIG_ENDIAN__){
            mask = (~0ul) << shift;
        } else {
            mask = (~0ul) >> shift;
        }
        return (value & mask) % HASH_TABLE_SIZE;
    } else {
        uint32_t value = *reinterpret_cast<const uint32_t*>(str);

        // Shift: Discard bits after len 
        uint32_t shift =  ((4 -  len     ) << 3);
        uint32_t mask;
        if (__BYTE_ORDER__ == __ORDER_BIG_ENDIAN__){
            mask = (~0u) << shift;
        } else {
            mask = (~0u) >> shift;
        }

        return  (value & mask) % HASH_TABLE_SIZE;
    }
}

inline uint8_t hash_db_slow(const char *str, unsigned int len){
    if (len >= 7) return hash_db(str, len);

    uint64_t value = 0;
    for (unsigned int i =0; i < len; i++)
        value |= (uint64_t)(str[i]) << 8*(i);
    return value % HASH_TABLE_SIZE;
}
struct QueryState;
void print_inq_state(const QueryState &st);
struct QueryState {
    const QueryExpr * expr_table;
    const char *json;
    ExprMask app_mask = 0; //applicatable
    ExprMask pred_mask = 0; //predicate
    ExprMask inv_mask = 0; //invoice
    ExprMask rec_mask = 0; //recusive
    ExprMask global_complete_mask = 0; //recusive

    QueryLayer *current; //Current layer
    InqSite site[MAX_SITE_DEPTH];
    QueryLayer layers[MAX_DEPTH];
    ExprMask hash_table[HASH_TABLE_SIZE+1];

    QueryState (const QueryExpr *ex, int expr_table_len,const char *json):
        expr_table{ex}, json{json}, current{layers}{
            for (int i = 0; i < expr_table_len; i++){
                if (ex[i].type_mask & EXPR_FLAG::INV) inv_mask |= 1ul << i;
                if (ex[i].type_mask & EXPR_FLAG::REC) rec_mask |= 1ul << i;
                if (ex[i].type_mask & EXPR_FLAG::APP) app_mask |= 1ul << i;
                if (ex[i].type_mask & EXPR_FLAG::PRED) pred_mask |= 1ul << i;
                if (ex[i].type_mask & EXPR_FLAG::APP){
                    hash_table[hash_db_slow(ex[i].name, ex[i].name_len)] |= (1ul << i);
                }
            }

    }
    InqSite &get_site(int index){
        return site[current->site_depth + bit_index(current->expr_mask & inv_mask, index)];
    }

    InqSite &get_site(int index, const QueryLayer *layer){
        return site[layer->site_depth + bit_index(layer->expr_mask & inv_mask, index)];
    }

    void check_raise(const  QueryExpr &expr, QueryLayer *layer){
        if (layer != layers && expr.type_mask & EXPR_FLAG::RAISE){
            set(layer-1, raise_index(expr.type_mask));
        }
    }

    inline uint64_t found_inv(const QueryLayer *layer){
        return (~layer->active_app_mask) & inv_mask & layer->expr_mask & ~(global_complete_mask);
    }

    inline uint64_t unpassed_meta_pred(const QueryLayer *layer){
        return (~layer->passed_mask) & (~app_mask) & pred_mask & layer->expr_mask;
    }

    //  
    void check_related(const QueryExpr &expr, QueryLayer *layer){
        //Check if any related found invoice are now satisfied.
       // printf("checking:%lu\n",layer - layers);
        if (expr.type_mask & EXPR_FLAG::RAISE_RELATED){
            if (layer == layers) return;
            for (auto i: IterateMask{expr.related}){
                set(layer-1, i);
            }
            return;
        }
        
        for (auto i: IterateMask{expr.related & found_inv(layer)}){
        //    printf("checking-index:%lu\n",i);
            check_invoice(layer, i);
        }

        //Check if any related meta predicates are now satisfied.
        for (auto i: IterateMask{expr.related & unpassed_meta_pred(layer)})
            check_pred(layer, i);

        //raise
        check_raise(expr, layer);
    }

    
    void set(QueryLayer *layer, int index){
        if ((layer->expr_mask & ~(layer->passed_mask)) & (1ul << index)){
            //Layer has unpassed expr at index.

            layer->passed_mask |= (1ul << index);

            check_related(expr_table[index], layer);
            if ((1ul << index) & pred_mask) check_pred(layer, index);
            if ((1ul << index) & inv_mask) {
                check_invoice(layer, index);
            }
        }
    }
    
    void check_invoice(const QueryLayer *layer, const int index){

        const QueryExpr &expr = expr_table[index];
        if (!((1ul << index) & layer->passed_mask)){
            
        if (expr.type_mask & EXPR_FLAG::OR){
            if (!(expr.related & layer->passed_mask))
                return;
        } else{
            if ((expr.related & layer->passed_mask) != expr.related)
                return;
        }

        }

        global_complete_mask |= (1ul << index);
         // printf("setting:%s:[layer:%lu, site-depth:%d, offset:%d]",expr.name,layer-layers,layer->site_depth,get_site(index,layer).pos);
         //  printf(" site-index:%d----\n", layer->site_depth + bit_index(layer->expr_mask & inv_mask, index));
        //    printf("%d\n",get_site(index,layer).pos);
        const char *json_rep = json + get_site(index,layer).pos;
        switch ((expr.type_mask & (~EXPR_FLAG::FLAG_SCOPE))){
            case EXPR_FLAG::STRING:
                *(expr.str_dest) = const_cast<char *>(json_rep);
                break;
            case EXPR_FLAG::INT:
                *(expr.int_dest) = parse_int(json_rep);
                break;
            case EXPR_FLAG::UINT:
                *(expr.uint_dest) = parse_uint(json_rep);
                break;
        }
    }

inline   void check_pred(const QueryLayer *layer, const int index){
        const QueryExpr &expr = expr_table[index];
        if (expr.type_mask & (EXPR_FLAG::APP | EXPR_FLAG::DUMMY)){
            if ((expr.related & (~global_complete_mask)) == 0){
                global_complete_mask |= (1ul << index);
            }
        }
    }
   
    void apply(int index, const char *pos){
        const QueryExpr &expr = expr_table[index];
        if (expr.type_mask & EXPR_FLAG::INV){
            get_site(index, current).pos = pos - json;
            check_invoice(current, index);
            return;
        }
        switch ((expr.type_mask & (~EXPR_FLAG::FLAG_SCOPE))){
            case EXPR_FLAG::BOOL:
                if (!compare_bool(expr.value, pos)) return;
                break;
            case EXPR_FLAG::STRING_PREFIX:
                if (*pos != '"') return;
                if (!startsWith(expr.str_value, pos+1,
                                jstr_skip(pos) - pos - 1)) return;
                //char_ref(json,pos);
                break;
            case EXPR_FLAG::UINT:
                if (parse_uint(pos) != expr.value) return;
                break;
        }

        current->passed_mask |= (1ul << index);

        check_related(expr, current);

        if ((expr.related & (~global_complete_mask)) == 0){
            global_complete_mask |= (1ul << index);
        }
    }

    bool recurse(ExprMask layer_ex, char entry){
        current++;

        if (current - layers >= MAX_DEPTH){
            return false;
        }
        current->passed_mask = 0;
        current->active_app_mask = layer_ex & app_mask;
        current->expr_mask = layer_ex;
        if (current != layers){
            current->site_depth = current[-1].site_depth + current[-1].site_len;
        } else {
            current->site_depth = 0;
        }
        current->site_len = __builtin_popcountl(current->expr_mask & inv_mask);

        if (current->site_depth + current->site_len >= MAX_SITE_DEPTH){
            return false;
        }
        current->array = (entry == '[');
        return true;
    }
    
};

#if PREDIQUERY_DEBUG
void print_inq_state(const QueryState &st){
    printf("Query_State[depth:%lu,site-depth:%d,site-len:%d",st.current - st.layers,st.current->site_depth,st.current->site_len);
    if (st.current->array){
       printf(",ARRAY"); 
    }
    printf("\n      GCM:");
        printbb((uint32_t)st.global_complete_mask);
    printf("]{\n");
    for (auto i: IterateMask{st.current->expr_mask}) {
        const QueryExpr &expr = st.expr_table[i];
        if (expr.type_mask & EXPR_FLAG::APP){
        printf("    %lu:\"%s\"(%c) -> [",i,
               st.expr_table[i].name,
               (st.current->active_app_mask & ((1ul << i))) ? ' ' : 'F'
        );
        }else{
          printf("    %lu:META(%c)   [",i, (st.current->passed_mask & ((1ul << i))) ? '1' : '0');
        }
        if (st.expr_table[i].type_mask & EXPR_FLAG::PRED){
            switch ((st.expr_table[i].type_mask & (~EXPR_FLAG::FLAG_SCOPE))){
                case EXPR_FLAG::UINT:
                    printf(" type:INT=%lu", expr.value);
                    break;
            }
            if (st.current->passed_mask & ((1ul << i))){
               printf(":->Passed"); 
            } else {
               printf(":->Failed"); 
            }
        } else if (expr.type_mask & EXPR_FLAG::REC){
            printf(" type:REC");
        } else if (expr.type_mask & EXPR_FLAG::INV){
            printf(" type:INV");
            printf(" site-index:%d", st.current->site_depth + bit_index(st.current->expr_mask & st.inv_mask, i));
        }
        printf(" ]");
        printf(" \n");
    }
    printf("}\n");
}
#endif 



uint64_t tquery(const char *json, const QueryExpr *expr_table,
                int expr_table_len, ExprMask root_ex){
    QueryState st {expr_table, expr_table_len, json};
    st.current = st.layers;
    st.current->active_app_mask = root_ex & st.app_mask;
    st.current->expr_mask = root_ex;

    const char * q = json;
    const char * next; //next 
    ExprMask rec_expr_map;
    int len;
    uint8_t hash;

    if (! *q) return 0;
    while (1){
        switch (*(++q)){
        case '\0':
            return st.global_complete_mask;
        case '"':
            next = jstr_skip(q);
            ++q;

            len = next - q;

            if (*(next = skip_whitespace(next+1)) != ':') { 
#if PREDIQUERY_DEBUG
                printf("Find_next_colon error by:");
                char_ref(json, q);
#endif  
                return 0;
            }

            if (!(*(next = skip_whitespace(next+1)))) {
#if PREDIQUERY_DEBUG
                printf("skip_space error by:");
                char_ref(json, q);
#endif
                return 0;
            }

            // Since `next` is at least 3 characters, skipped '"' and ':',
            // hashin `q` will only access memory from 'json' and thus owned.   
            hash = hash_db(q, len);

            rec_expr_map = 0;
            for(auto i: IterateMask{st.current->active_app_mask & st.hash_table[hash]}){
                const QueryExpr &expr = expr_table[i];
                if (len == expr.name_len && strncmp(expr.name, q, len) == 0){
                    st.current->active_app_mask &= ~(1ul << i);
                    if (expr.type_mask & EXPR_FLAG::REC) {
                        rec_expr_map = expr.related;
                        continue; 
                    };
                    st.apply(i, next);
                } 
            }

            if (!(st.inv_mask & (~st.global_complete_mask))) {
//                print_inq_state(st);
                //Terminate early if all the invoices are satisfied. 
                return st.global_complete_mask;
            };

            q = next;
            if (rec_expr_map && (*next == '{' || *next == '[')){
                if (st.recurse(rec_expr_map & ~st.global_complete_mask, *next)){
                    continue;
                } else { //Max Depth exceeded
                    return 0;
                }
                //print_inq_state(st);
            }else{
                if (!st.current->active_app_mask){
                    q = query_skip_dep(q, 1);
                }else{
                    q = query_skip(q);
                }
                //Query Skip should only return when q is ',' '}' '\0' or ']'.
                switch (*q){
                    case ',': continue; 
                    case '}': goto obj_end_label;
                    case ']': goto array_end_label;
                    default: //Reach end of json
                        return st.global_complete_mask;
                }
            }
            continue;
            case '}':
        obj_end_label:
//            print_inq_state(st);
            //Array layers reuse the same object.

            if (!st.current->array) {
                if (st.current == st.layers)
                    return st.global_complete_mask;
                --st.current;
            }
            continue;
        case ']':
        array_end_label:
            if (st.current->array != true){
#if PREDIQUERY_DEBUG
                printf("Unexpect array exit:\n");
                char_ref(json,q);
                char_ref(json,next);
#endif
                return 0;
            }
            --st.current;
            continue;
            case '{': 
            if (st.current == st.layers) continue;
            if (st.current->array != true){
#if PREDIQUERY_DEBUG
                printf("Unexpect OBJ entry:\n");
                char_ref(json,next);
#endif
                return 0;
            }
            st.current->passed_mask =0;
            st.current->active_app_mask = st.current->expr_mask & st.app_mask & (~st.global_complete_mask);
            //print_inq_state(st);
            continue;
         default:
            continue;
    }
    }
    return 0;
}
    
} //namespace prediquery


#endif // __PREDIQUERY_H
